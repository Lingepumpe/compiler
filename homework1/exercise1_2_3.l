%{
#include <stdio.h>
int tokens = 0;
int block_line_begin = -1;
%}
%x IN_COMMENT
%x IN_STRING
%option yylineno


%%
<INITIAL>{
"/*"        ++tokens; fprintf(stderr, "MULTI_LINE_COMMENT "); BEGIN(IN_COMMENT); block_line_begin = yylineno;
\"          ++tokens; fprintf(stderr, "STRING "); BEGIN(IN_STRING); block_line_begin = yylineno;
}
<IN_COMMENT>{
"*/"        BEGIN(INITIAL);
[^*\n]+     //eat comments in chunks
"*"         //eat the lone star
\n          yylineno++;
<<EOF>>     fprintf(stderr, "\nERROR: Unterminated multi line comment, begins in line %d.\n", block_line_begin); return 0;
}
<IN_STRING>{
\\.                 /*consume escaped characters*/
[^"\n\\]+           //eat strings in chunks
\"                  BEGIN(INITIAL);
\n                  yylineno++;
<<EOF>>     fprintf(stderr, "\nERROR: Unterminated string literal, begins in line %d.\n", block_line_begin); return 0;
}
\/\/[^\n]*\n        ++tokens; fprintf(stderr, "ONE_LINE_COMMENT\n"); yylineno++;
\{                  ++tokens; fprintf(stderr, "O_BRACE ");
\}                  ++tokens; fprintf(stderr, "C_BRACE ");
\[                  ++tokens; fprintf(stderr, "O_BRACKET ");
\]                  ++tokens; fprintf(stderr, "C_BRACKET ");
\(                  ++tokens; fprintf(stderr, "O_PARENTHESIS ");
\)                  ++tokens; fprintf(stderr, "C_PARENTHESIS ");
float|int|boolean   ++tokens; fprintf(stderr, "TYPE(%s) ", yytext);
if|while|for|write|final  ++tokens; fprintf(stderr, "KEYWORD(%s) ", yytext);
[=+\-*/<>!&%]       ++tokens; fprintf(stderr, "OPERATOR(%s) ", yytext);
;                   ++tokens; fprintf(stderr, "SEMICOLON ");
,                   ++tokens; fprintf(stderr, "COMMA ");
[0-9]+              ++tokens; fprintf(stderr, "NUMBER(%s) ", yytext);
true|false          ++tokens; fprintf(stderr, "CONSTANT(%s) ", yytext);
[a-zA-Z_][a-zA-Z0-9_]* ++tokens; fprintf(stderr, "IDENTIFIER(%s) ", yytext);
[ ]                 /*consume excess whitespace*/ fprintf(stderr, " ");
[\t]                /*consume excess whitespace*/ fprintf(stderr, "\t");
[\r]                {}
[\n]                fprintf(stderr, "\n");
.                   fprintf(stderr, "\nERROR: Lexer error in line %d: \"%s\"\n", yylineno, yytext);
%%

int main() {
  yylex();
  printf("\nNumber of tokens: %d\n", tokens);
  return 0;
}
