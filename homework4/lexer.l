%{
#include <stdio.h>
#include "tokens.h"

int block_line_begin = -1;



%}
%x IN_COMMENT
%x IN_MODULA_COMMENT
%x IN_STRING
%option yylineno
%option noyywrap

%%
<INITIAL>{
"/*"        BEGIN(IN_COMMENT); block_line_begin = yylineno;
\"          BEGIN(IN_STRING); block_line_begin = yylineno;
}
<IN_COMMENT>{
"*/"        BEGIN(INITIAL);
[^*\n]+     //eat comments in chunks
"*"         //eat the lone star
\n          yylineno++;
<<EOF>>     return LEXER_ERROR_UNCLOSED_COMMENT;
}

<IN_STRING>{
\\.                 /*consume escaped characters*/
[^"\n\\]+           //eat strings in chunks
\"                  BEGIN(INITIAL); return STRING;
\n                  yylineno++;
<<EOF>>             return LEXER_ERROR_UNCLOSED_STRING;
}


\/\/[^\n]*\n        yylineno++;
\{                  return O_BRACE;
\}                  return C_BRACE;
\[                  return O_BRACKET;
\]                  return C_BRACKET;
\(                  return O_PAREN;
\)                  return C_PAREN;
float|int|boolean   return TYPE;
if                  return IF;
else                return ELSE;
while               return WHILE;
for                 return FOR;
final               return FINAL;
read|write          return IOFUNC;
"="                 return EQUALS;
"<"                 return SMALLER;
">"                 return GREATER;
"+"                 return PLUS;
"-"                 return MINUS;
"*"                 return STAR;
"/"                 return SLASH;
"%"                 return PERCENT;
"!"                 return EXCLAMATION_MARK;
"&"                 return AND;
"|"                 return OR;
";"                 return COLON;
","                 return COMMA;
[0-9]+              return NUMBER;
true|false          return TRUTH_VAL;
[a-zA-Z_][a-zA-Z0-9_]* return IDENT;
[ ]                 {} /*consume excess whitespace*/
[\t]                {} /*consume excess whitespace*/
[\r]                {} /*consume excess whitespace*/
[\n]                {} /*consume excess whitespace*/
<<EOF>>             return END_OF_FILE;
.                   return LEXER_ERROR;
%%

