%{
#include <stdio.h>
#include "ast_types.h"
#include "lalr1_yacc.tab.h"

int block_line_begin = -1;

%}
%x IN_COMMENT
%x IN_MODULA_COMMENT
%x IN_STRING
%option yylineno


%%
<INITIAL>{
"/*"        BEGIN(IN_COMMENT); block_line_begin = yylineno;
\"          BEGIN(IN_STRING); block_line_begin = yylineno;
}
<IN_COMMENT>{
"*/"        BEGIN(INITIAL);
[^*\n]+     //eat comments in chunk
"*"         //eat the lone star
\n          yylineno++;
<<EOF>>     return LEXER_ERROR_UNCLOSED_COMMENT;
}

<IN_STRING>{
\\.                 addToString(yytext); /*consume escaped characters*/
[^"\n\\]+           addToString(yytext); //eat strings in chunks
\"                  yylval.t.type = STRING_CONST; yylval.t.identifier = string_buf; string_buf = NULL; BEGIN(INITIAL); return LEXER_STRING;
\n                  addToString(yytext); yylineno++;
<<EOF>>             free(string_buf); return LEXER_ERROR_UNCLOSED_STRING;
}


\/\/[^\n]*\n           yylineno++;
\{                     yylval.t.type = NONE; return LEXER_O_BRACE;
\}                     yylval.t.type = NONE; return LEXER_C_BRACE;
\[                     yylval.t.type = NONE; return LEXER_O_BRACKET;
\]                     yylval.t.type = NONE; return LEXER_C_BRACKET;
\(                     yylval.t.type = NONE; return LEXER_O_PAREN;
\)                     yylval.t.type = NONE; return LEXER_C_PAREN;
float|int|boolean      yylval.t = leafNodeIdent(TYPE, strndup(yytext, 16)); ; return LEXER_TYPE;
if                     yylval.t.type = NONE; return LEXER_IF;
else                   yylval.t.type = NONE; return LEXER_ELSE;
while                  yylval.t.type = NONE; return LEXER_WHILE;
for                    yylval.t.type = NONE; return LEXER_FOR;
final                  yylval.t = leafNodeIdent(FINAL, strndup("final", 16)); return LEXER_FINAL;
read|write             yylval.t = leafNodeIdent(IOFUNC, strndup(yytext, 16)); return LEXER_IOFUNC;
"<="                   yylval.t = leafNodeInt(OP, LE); return LEXER_SMALLER_EQUALS;
">="                   yylval.t = leafNodeInt(OP, GE); return LEXER_GREATER_EQUALS;
"<"                    yylval.t = leafNodeInt(OP, LT); return LEXER_SMALLER;
">"                    yylval.t = leafNodeInt(OP, GT); return LEXER_GREATER;
"=="                   yylval.t = leafNodeInt(OP, EQ); return LEXER_EQUALS;
"!="                   yylval.t = leafNodeInt(OP, NE); return LEXER_NOT_EQUALS;
"="                    yylval.t.type = NONE; return LEXER_ASSIGN;
"++"                   yylval.t = leafNodeInt(OP, PLUSPLUS); return LEXER_PLUS_PLUS;
"--"                   yylval.t = leafNodeInt(OP, MINUSMINUS); return LEXER_MINUS_MINUS;
"+"                    yylval.t = leafNodeInt(OP, PLUS); return LEXER_PLUS;
"-"                    yylval.t = leafNodeInt(OP, MINUS); return LEXER_MINUS;
"*"                    yylval.t = leafNodeInt(OP, MUL); return LEXER_STAR;
"/"                    yylval.t = leafNodeInt(OP, DIV); return LEXER_SLASH;
"%"                    yylval.t = leafNodeInt(OP, MOD); return LEXER_PERCENT;
"!"                    yylval.t = leafNodeInt(OP, NOT); return LEXER_EXCLAMATION_MARK;
"&&"                   yylval.t = leafNodeInt(OP, AND); return LEXER_AND;
"||"                   yylval.t = leafNodeInt(OP, OR); return LEXER_OR;
";"                    yylval.t.type = NONE; return LEXER_COLON;
","                    yylval.t.type = NONE; return LEXER_COMMA;
[0-9]+                 yylval.t = leafNodeInt(INT_CONST, atoi(yytext)); return LEXER_NUMBER;
true|false             yylval.t = leafNodeInt(BOOL_CONST, yytext[0] == 't' ? 1 : 0); return LEXER_TRUTH_VAL;
[a-zA-Z_][a-zA-Z0-9_]* yylval.t = leafNodeIdent(IDENTIFIER, strndup(yytext, 64)); return LEXER_IDENT;
[ ]                    {} /*consume excess whitespace*/
[\t]                   {} /*consume excess whitespace*/
[\r]                   {} /*consume excess whitespace*/
[\n]                   {} /*consume excess whitespace*/
<<EOF>>                yylval.t.type = NONE; return 0;
.                      yylval.t.type = NONE; return LEXER_ERROR;
%%

int yywrap() { return 1; }


