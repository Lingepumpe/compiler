#ifndef LEX_FUNCS_H
#define LEX_FUNCS_H

#include <stdio.h>
#include <string.h>

extern "C"
{
    extern int yylineno;
    extern int block_line_begin;
    
    int yylex();
}

#endif //LEX_FUNCS_H
