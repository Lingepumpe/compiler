#!/bin/bash

set -x
set -e

CPP="g++"
CC="gcc"
OPTS="-W -ggdb"

lex lexer.l
${CC}  ${OPTS} -c lex.yy.c
${CPP} ${OPTS} -std=c++0x lex.yy.o top_down_parser.cpp -o tdp

