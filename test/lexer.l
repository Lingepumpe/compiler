%{
#include <stdio.h>
#include "lalr1_yacc.tab.h"


%}
%option yylineno
%option noyywrap

%%
[0-9]+              yylval.val = atoi(yytext); return NUMBER;
[*]                 return MUL;
[+]                 return ADD;
[ ]                 {} /*consume excess whitespace*/
[\t]                {} /*consume excess whitespace*/
[\r]                {} /*consume excess whitespace*/
[\n]                {} /*consume excess whitespace*/
<<EOF>>             return 0;
.                   return LEXER_ERROR;
%%

