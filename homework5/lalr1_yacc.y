%{
#include <stdio.h>

int yylex();
int yyerror(const char* s);

extern int yylineno;
%}

%token O_BRACE C_BRACE O_BRACKET C_BRACKET O_PAREN C_PAREN EQUALS SMALLER GREATER NUMBER STR COLON FINAL TYPE COMMA IDENT IOFUNC WHILE FOR IF ELSE PLUS MINUS STAR SLASH PERCENT EXCLAMATION_MARK AND OR STRING TRUTH_VAL END_OF_FILE LEXER_ERROR LEXER_ERROR_UNCLOSED_COMMENT LEXER_ERROR_UNCLOSED_STRING
%start program
%error-verbose

%%

program         : compStmt;
compStmt        :
                | O_BRACE varDecList stmtList C_BRACE compStmt;
varDecList      :
                | varDecList varListType COLON;
varListType     : final TYPE varList
final           :
                | FINAL;
varList         : varList COMMA ident
                | ident
                | ident EQUALS expr;
ident           : IDENT
                | IDENT O_BRACKET simpleExpr C_BRACKET;
stmtList        : stmtList statement
                | statement;
statement       : assignStmt COLON
                | compStmt
                | ifStmt
                | whileStmt
                | forStmt
                | ioStmt COLON;
assignStmt      : ident EQUALS expr
                | ident PLUS PLUS
                | ident MINUS MINUS;
ifStmt          : IF O_PAREN expr C_PAREN statement elsePart;
elsePart        :
                | ELSE statement;
whileStmt       : WHILE O_PAREN expr C_PAREN statement;
forStmt         : FOR O_PAREN assignStmt COLON expr COLON assignStmt C_PAREN statement;
ioStmt          : IOFUNC O_PAREN paramList C_PAREN;
paramList       : paramList COMMA expr
                | paramList COMMA STRING
                | expr
                | STRING;
expr            : simpleExpr relOp simpleExpr
                | simpleExpr;
simpleExpr      : term addOp simpleExpr
                | term;
term            : term mulOp factor
                | factor;
factor          : NUMBER
                | TRUTH_VAL
                | ident
                | EXCLAMATION_MARK factor
                | MINUS factor
                | O_PAREN factor C_PAREN;
relOp           : SMALLER | SMALLER EQUALS | GREATER | GREATER EQUALS | EXCLAMATION_MARK EQUALS | EQUALS EQUALS;
addOp           : PLUS | MINUS | OR OR;
mulOp           : STAR | SLASH | PERCENT | AND AND;

%%
int main() {
    return yyparse();
}

int yyerror(const char* s) {
    fprintf(stderr, "%s, line ~%d\n", s, yylineno);
    return 0;
}
