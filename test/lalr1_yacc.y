%{
#include <stdio.h>
#include <assert.h>

int yylex();
int yyerror(const char* s);

extern int yylineno;

struct term_underscore;
struct term_underscore {
    int op; //-1 for empty term_underscore
    int fact;
    struct term_underscore* t_;
};

%}

%token NUMBER MUL ADD LEXER_ERROR
%start term
%error-verbose
%union { int val; struct term_underscore* t_val; }
%type <val> term op fact
%type <val> NUMBER
%type <t_val> term_
%%

term            : fact term_ {
                  int res = $1;
                  struct term_underscore* cur = $2;
                  while(cur->op != -1) {
                      if(cur->op == MUL) {
                          res = res * cur->fact;
                      } else {
                          assert(cur->op == ADD);
                          res = res + cur->fact;
                      }
                      cur = cur->t_;
                  }
                  printf("Result is: %d\n", res);
                };
term_           : op fact term_ { $$ = malloc(sizeof(struct term_underscore)); $$->op = $1; $$->fact = $2; $$->t_ = $3; }
                | { $$ = malloc(sizeof(struct term_underscore)); $$->op = -1; $$->t_ = NULL;};
op              : MUL { $$ = MUL; }
                | ADD { $$ = ADD; };
fact            : NUMBER { $$ = $1; printf("%d\n", $$); };

%%
int main() {
    return yyparse();
}

int yyerror(const char* s) {
    fprintf(stderr, "%s, line ~%d\n", s, yylineno);
    return 0;
}
