#include <iostream>
#include <stdlib.h>
#include <vector>
#include <assert.h>

#include "tokens.h"
#include "lex_funcs.h"


#define DEBUG
#define PARSECALL(x) { std::string prev = currentFunc; currentFunc = #x; x(); currentFunc = prev; }

class TopDownParser {
    std::string currentFunc;
    int         reservedToken;

    const char* getTokenString(int token) {
        //generated via sed 's/,//g' | sed 's/ *//g' | sed 's/^/"/' | sed 's/$/", /' | tr -d "\n"
        static const char* tokenToString[] = { "<eof>", "O_BRACE", "C_BRACE", "O_BRACKET", "C_BRACKET", "O_PAREN", "C_PAREN", "EQUALS", "SMALLER", "GREATER", "NUMBER", "STR", "COLON", "FINAL", "TYPE", "COMMA", "IDENT", "IOFUNC", "WHILE", "FOR", "IF", "ELSE", "PLUS", "MINUS", "STAR", "SLASH", "PERCENT", "EXCLAMATION_MARK", "AND", "OR", "STRING", "TRUTH_VAL" };
        return tokenToString[token];
    }
    
    int getToken() {
        if(reservedToken >= 0) {
            int result = reservedToken;
            reservedToken = -1;
            return result;
        }
            
        int token = yylex();
#ifdef DEBUG
        std::cout << getTokenString(token) << std::endl;
#endif
        if(token < 0) {
            if(token == LEXER_ERROR) {
                std::cerr << "Lexer error around line " << yylineno << std::endl;
            } else if(token == LEXER_ERROR_UNCLOSED_COMMENT) {
                std::cerr << "Lexer error: Comment never closed, comment started around line " << block_line_begin << std::endl;
            } else if(token == LEXER_ERROR_UNCLOSED_STRING) {
                std::cerr << "Lexer Error: String never closed, string begins around line " << block_line_begin << std::endl;
            }
            exit(token);
        }
        return token;
    }

    int match(const std::vector<int>& oneOf) {
        int token;
        if(softMatch(oneOf, &token)) {
            return token;
        }
        std::cerr << "Parse error: In \"" << currentFunc << "()\", expected " << getTokenString(oneOf.front());
        for(std::vector<int>::const_iterator iter = ++(oneOf.begin()); iter != oneOf.end(); ++iter) {
            std::cerr << " or " << getTokenString(*iter); 
        }
        std::cerr << ", but got " << getTokenString(token) << std::endl;
        std::cerr << "While parsing around line " << yylineno << std::endl;
        exit(-1);
    }

    int match(int expectedToken) {
        std::vector<int> v = {expectedToken};
        return match(v);
    }

    bool softMatch(const std::vector<int>& oneOf, int* matchedToken) {
        assert(oneOf.size() > 0);
        *matchedToken = getToken();
        for(std::vector<int>::const_iterator iter = oneOf.begin(); iter != oneOf.end(); ++iter) {
            if(*matchedToken == *iter) {
                return true;
            }
        }
        returnToken(*matchedToken);
        return false;
    }

    void returnToken(int token) {
        assert(token >= 0);
        assert(reservedToken < 0);
        reservedToken = token;
    }
    
/*************** gramar parsing funcs ******************/
    //assignStmt''  -> EQUALS expr {EQUALS expr} | O_BRACKET expr C_BRACKET EQUALS expr {EQUALS expr} | PLUS PLUS
    void assignStmt__() {
        int m = match({EQUALS, O_BRACKET, PLUS});
        if(m == EQUALS) {
            PARSECALL(expr);
            while(softMatch({EQUALS}, &m)) {
                PARSECALL(expr);
            }
        } else if(m == O_BRACKET) {
            PARSECALL(expr);
            match(C_BRACKET);
            match(EQUALS);
            PARSECALL(expr);
            while(softMatch({EQUALS}, &m)) {
                PARSECALL(expr);
            }
        } else {
            assert(m == PLUS);
            match(PLUS);
        }
    }
    
    //assignStmt    -> IDENT assignStmt''
    void assignStmt() {
        match(IDENT);
        PARSECALL(assignStmt__);
    }

    //paramList     -> IDENT [O_BRACKET expr C_BRACKET] [COMMA paramList] | STRING [COMMA paramList] | epsilon
    void paramList() {
        int m;
        if(softMatch({IDENT, STRING}, &m)) {
            if(softMatch({O_BRACKET}, &m)) {
                PARSECALL(expr);
                match(C_BRACKET);
            }
            if(softMatch({COMMA}, &m)) {
                PARSECALL(paramList);
            }
        }
        
    }

    //varList       -> IDENT [O_BRACKET expr C_BRACKET] [assignStmt''] {COMMA varList}
    void varList() {
        int m;
        match(IDENT);
        if(softMatch({O_BRACKET}, &m)) {
            PARSECALL(expr);
            match(C_BRACKET);
        }
        if(softMatch({EQUALS, O_BRACKET}, &m)) { //assignStmt__ needs = or [
            returnToken(m); //must return this token since it will be parsed by assignStmt__
            PARSECALL(assignStmt__);
        }
        if(softMatch({COMMA}, &m)) {
            PARSECALL(varList);
        }
    }

    //factor'       -> epsilon | O_BRACKET expr C_BRACKET
    void factor_() {
        int m;
        if(softMatch({O_BRACKET}, &m)) {
            assert(m == O_BRACKET);
            PARSECALL(expr);
            match(C_BRACKET);
        }
    }
    
    //factor        -> NUM | TRUTH_VAL | IDENT factor' | EXCLAMATION_MARK factor | MINUS factor | O_PAREN factor C_PAREN
    void factor() {
        int m;
        m = match({NUMBER, TRUTH_VAL, IDENT, EXCLAMATION_MARK, MINUS, O_PAREN});
        if(m == IDENT) {
            PARSECALL(factor_);
        } else if(m == EXCLAMATION_MARK || m == MINUS) {
            PARSECALL(factor);
        } else if(m == O_PAREN) {
            PARSECALL(factor);
            match(C_PAREN);
        }
    }
    
    //term          -> factor {mulOp factor}
    //STAR | SLASH | PERCENT | AND AND
    void term() {
        int m;
        PARSECALL(factor);
        while(softMatch({STAR, SLASH, PERCENT, AND}, &m)) {
            if(m == AND) {
                match(AND);
            }
            PARSECALL(term);
        }
    }

    
    //simpleExpr    -> term {addOp term}
    //PLUS | MINUS | OR OR
    void simpleExpr() {
        int m;
        PARSECALL(term);
        while(softMatch({PLUS, MINUS, OR}, &m)) {
            if(m == OR) {
                match({OR});
            }
            PARSECALL(term);
        }
    }
    
    //expr          -> simpleExpr {relOp simpleExpr}
    //< | <= | > | >= | == | !=
    void expr() {
        int m;
        PARSECALL(simpleExpr);
        while(softMatch({SMALLER, GREATER, EQUALS, EXCLAMATION_MARK}, &m)) {
            if(m == SMALLER || m == GREATER) {
                softMatch({EQUALS}, &m); //check if it is <= or only <, or >= or only >
            } else if(m == EQUALS) {
                match(EQUALS);
            } else {
                assert(m == EXCLAMATION_MARK);
                match(EQUALS);
            }
            PARSECALL(simpleExpr);
        }
    }

    void statement() {
        int m = match({IDENT, IOFUNC, FINAL, TYPE, O_BRACE, IF, WHILE, FOR});
        if(m == IDENT) {
            PARSECALL(assignStmt__);
            match(COLON);
        } else if(m == IOFUNC) {
            match(O_PAREN);
            PARSECALL(paramList);
            match(C_PAREN);
            match(COLON);
        } else if(m == FINAL) {
            match(TYPE);
            PARSECALL(varList);
            match(COLON);
        } else if(m == TYPE) {
            PARSECALL(varList);
            match(COLON);
        } else if(m == O_BRACE) {
            PARSECALL(stmtList);
            match(C_BRACE);
        } else if(m == IF) {
            match(O_PAREN);
            PARSECALL(expr);
            match(C_PAREN);
            PARSECALL(statement);
            int m;
            if(softMatch({ELSE}, &m)) {
                PARSECALL(statement);
            }
        } else if(m == WHILE) {
            match(O_PAREN);
            PARSECALL(expr);
            match(C_PAREN);
            PARSECALL(statement);
        } else {
            assert(m == FOR);
            match(O_PAREN);
            PARSECALL(assignStmt);
            match(COLON);
            PARSECALL(expr);
            match(COLON);
            PARSECALL(assignStmt);
            match(C_PAREN);
            PARSECALL(statement);
        }
    }

//stmtList      -> {IDENT assignStmt'' COLON | iocall(paramList) COLON | FINAL TYPE varList COLON | TYPE varList COLON | O_BRACE stmtList C_BRACE | IF O_PAREN expr C_PAREN statement elsePart | WHILE O_PAREN expr C_PAREN statement | FOR O_PAREN assignStmt COLON expr COLON assignStmt C_PAREN statement}
    void stmtList() {
        int m;
        while(softMatch({IDENT, IOFUNC, FINAL, TYPE, O_BRACE, IF, WHILE, FOR}, &m)) { //these are the first tokens for statements
            returnToken(m); //must gives back the first token for statement to parse it
            PARSECALL(statement);
        }
    }
            
    void stmtBlock() {
        match(O_BRACE);
        PARSECALL(stmtList);
        match(C_BRACE);
        match(END_OF_FILE);
    }
    
    void program() {
        PARSECALL(stmtBlock);
    }

public:
    TopDownParser() {
        reservedToken = -1;
        currentFunc = "ROOT";
    }
    void parse() {
        PARSECALL(program);
    }

};
    
int main() {
    TopDownParser tdp;
    tdp.parse();
}

