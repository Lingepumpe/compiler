%{
#include <stdio.h>

int words = 0;
%}

whitespace		[ \t\n]

%%

[^{whitespace}]+	words++;
[{whitespace}]+

%%

main(int argc, char **argv) {
  yylex();
  printf("\nNumber of words: %d\n", words);
  return 0;
}

