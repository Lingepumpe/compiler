#ifndef AST_TYPES_H
#define AST_TYPES_H

typedef enum {
    COMP_STMT = 0,
    ASSIGN,
    ASSIGN_STMT,
    IF,
    FOR,
    WHILE,
    STATEMENT,
    STATEMENT_LIST,
    CONST,
    VAR,
    VAR_LIST,
    TYPE,
    EXPR,
    INT_CONST,
    REAL_CONST,
    BOOL_CONST,
    STRING_CONST,
    IDENTIFIER,
    OP,
    IOFUNC,
    IOFUNCCALL,
    PARAM_LIST,
    FINAL,
    NONE,
} node_type;

typedef enum {
    PLUS = 0,
    MINUS,
    MUL,
    DIV,
    MOD,
    LT,
    LE,
    GT,
    GE,
    EQ,
    NE,
    AND,
    OR,
    PLUSPLUS,
    MINUSMINUS,
    NOT,
} operator;

typedef enum { _CONST=0, _SCALAR, _ARRAY } entry_type;
typedef enum { _BOOL=0, _INT, _FLOAT } data_type;

typedef struct _entry {
    entry_type etype;
    data_type dtype;
    union {
        int   int_val;
        float float_val;
        char *scalar;
        void *array[2]; /* identifier, index */
    } symbol;
    struct _entry *next; /* collision list */
} entry;

typedef struct _node {
    node_type type;
    entry **sym_tab; /* symbol table for COMP_STMT */
    struct _node *outer_scope; /* pointer to the outer block */
    union {
        operator op;
        entry *symbol; //if it is a variable it is stored here
        /* list of BNF right-hand side symbols of nonterminal type */
        struct _node *body;
    };
    struct _node* next; /* decl-list, stmt-list */
} node;

node makeNode1(int type, node body1);
node makeNode2(int type, node body1, node body2);
node makeNode3(int type, node body1, node body2, node body3);
node makeNode4(int type, node body1, node body2, node body3, node body4);
node leafNodeIdent(int type, char* identifier);
node leafNodeInt(int type, int value);
void addNext(node* to, node next);

extern char* string_buf;
extern int   string_buf_free_from;
extern int   string_buf_len;
void addToString(char* toAdd);


#endif //AST_TYPES_H
