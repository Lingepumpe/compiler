%{
#include <stdio.h>
int tokens = 0;
int block_line_begin = -1;

void lowerCase(char* string) {
  while(*string != '\0') {
    if(*string >= 'A' && *string <= 'Z') {
      *string = *string - 'A' + 'a';
    }
    ++string;
  }
}

%}
%x IN_COMMENT
%x IN_ONELINE_COMMENT
%x IN_STRING
%option yylineno
%%
<INITIAL>{
"/*"        ++tokens; fprintf(stderr, "/** "); BEGIN(IN_COMMENT); block_line_begin = yylineno;
\"          ++tokens; fprintf(stderr, "%s", yytext); BEGIN(IN_STRING); block_line_begin = yylineno;
\/\/        ++tokens; fprintf(stderr, "/** "); BEGIN(IN_ONELINE_COMMENT);
}

<IN_COMMENT>{
"*/"        fprintf(stderr, "%s", yytext); BEGIN(INITIAL);
\r          {}
[^*\n\r]+     fprintf(stderr, "%s", yytext); //eat comments in chunks
"*"         fprintf(stderr, "%s", yytext); //eat the lone star
\n          fprintf(stderr, "%s", yytext); yylineno++;
<<EOF>>     fprintf(stderr, "\nERROR: Unterminated multi line comment, begins in line %d.\n", block_line_begin); return 0;
}

<IN_ONELINE_COMMENT>{
\r          {}
[^\n\r]+      fprintf(stderr, "%s", yytext);
\n          fprintf(stderr, "*/\n"); BEGIN(INITIAL);
}

<IN_STRING>{
\\.                 fprintf(stderr, "%s", yytext); /*consume escaped characters*/
[^"\n\r\\]+         fprintf(stderr, "%s", yytext); //eat strings in chunks
\"                  fprintf(stderr, "%s", yytext); BEGIN(INITIAL);
\n                  fprintf(stderr, "%s", yytext); yylineno++;
\r                  {}
<<EOF>>             fprintf(stderr, "\nERROR: Unterminated string literal, begins in line %d.\n", block_line_begin); return 0;
}

\{                  ++tokens; fprintf(stderr, "%s", yytext);
\}                  ++tokens; fprintf(stderr, "%s", yytext);
\[                  ++tokens; fprintf(stderr, "%s", yytext);
\]                  ++tokens; fprintf(stderr, "%s", yytext);
\(                  ++tokens; fprintf(stderr, "%s", yytext);
\)                  ++tokens; fprintf(stderr, "%s", yytext);
float|int|boolean   ++tokens; fprintf(stderr, "%s", yytext);
if|while|for|write|final  ++tokens; fprintf(stderr, "%s", yytext);
[=+\-*/<>!&%]       ++tokens; fprintf(stderr, "%s", yytext);
;                   ++tokens; fprintf(stderr, "%s", yytext);
,                   ++tokens; fprintf(stderr, "%s", yytext);
[0-9]+              ++tokens; fprintf(stderr, "%s", yytext);
true|false          ++tokens; fprintf(stderr, "%s", yytext);
[a-zA-Z_][a-zA-Z0-9_]* ++tokens; lowerCase(yytext); fprintf(stderr, "%s", yytext);
[\r]                {} /*we really dont like \r's*/
[ \t\n][ \t\n]+     /*consume excess whitespace*/ fprintf(stderr, "%c", *yytext);
[ \t\n]             fprintf(stderr, "%s", yytext);
.                   fprintf(stderr, "\nERROR: Lexer error in line %d: \"%s\"\n", yylineno, yytext);
%%

int main() {
  yylex();
  printf("\nNumber of tokens: %d\n", tokens);
  return 0;
}
