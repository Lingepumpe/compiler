#!/bin/bash

set -x
set -e

CPP="g++"
CC="gcc"
OPTS="-W -ggdb --std=gnu99"

bison -v -d lalr1_yacc.y
lex lexer.l

${CC}  ${OPTS} -o lalr1_yacc lalr1_yacc.tab.c lex.yy.c -lfl

