%{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "ast_types.h"

int yylex();
int yyerror(const char* s);

extern int yylineno;

node tree_root;
%}

%token LEXER_O_BRACE LEXER_C_BRACE LEXER_O_BRACKET LEXER_C_BRACKET LEXER_O_PAREN LEXER_C_PAREN LEXER_NOT_EQUALS LEXER_SMALLER_EQUALS LEXER_GREATER_EQUALS LEXER_ASSIGN LEXER_EQUALS LEXER_SMALLER LEXER_GREATER LEXER_NUMBER LEXER_STR LEXER_COLON LEXER_FINAL LEXER_TYPE LEXER_COMMA LEXER_IDENT LEXER_IOFUNC LEXER_WHILE LEXER_FOR LEXER_IF LEXER_ELSE LEXER_PLUS_PLUS LEXER_MINUS_MINUS LEXER_PLUS LEXER_MINUS LEXER_STAR LEXER_SLASH LEXER_PERCENT LEXER_EXCLAMATION_MARK LEXER_AND LEXER_OR LEXER_STRING LEXER_TRUTH_VAL LEXER_END_OF_FILE LEXER_ERROR_UNCLOSED_COMMENT LEXER_ERROR_UNCLOSED_STRING LEXER_ERROR
%start program
%error-verbose

%union { node t; }

 //union {
 //   int iValue; /*integer, true, false, compOp, addOp, mulOp */
 //   float fValue; /*number*/
 //   char* identifier; /*identifier*/
 //   /* list of BNF right-hand side symbols of nonterminal type */
 //   struct _node *body;
 //};*/

 //%type <iValue>
 //%type <fValue>
 //%type <identifier>
 //%type <body>

%type <t> mulOp relOp addOp factor ident term simpleExpr expr paramList ioStmt forStmt whileStmt elsePart ifStmt assignStmt statement stmtList varList final varListType varDecList compStmt program
%type <t> LEXER_O_BRACE LEXER_C_BRACE LEXER_O_BRACKET LEXER_C_BRACKET LEXER_O_PAREN LEXER_C_PAREN LEXER_NOT_EQUALS LEXER_SMALLER_EQUALS LEXER_GREATER_EQUALS LEXER_ASSIGN LEXER_EQUALS LEXER_SMALLER LEXER_GREATER LEXER_NUMBER LEXER_STR LEXER_COLON LEXER_FINAL LEXER_TYPE LEXER_COMMA LEXER_IDENT LEXER_IOFUNC LEXER_WHILE LEXER_FOR LEXER_IF LEXER_ELSE LEXER_PLUS_PLUS LEXER_MINUS_MINUS LEXER_PLUS LEXER_MINUS LEXER_STAR LEXER_SLASH LEXER_PERCENT LEXER_EXCLAMATION_MARK LEXER_AND LEXER_OR LEXER_STRING LEXER_TRUTH_VAL

%nonassoc '+' '*'
%left '-'
%left '/'
%expect 39

%%
program         : compStmt { tree_root = $1; $$ = $1; };
compStmt        : { $$.type = NONE; }
                | LEXER_O_BRACE varDecList stmtList LEXER_C_BRACE compStmt {
                    if($2.type == NONE) {
                        $$ = makeNode1(COMP_STMT, $3);
                    } else {
                        $$ = makeNode2(COMP_STMT, $2, $3);
                    }
                    if($5.type != NONE) {
                        addNext(&$$, $5);
                    }
                };
varDecList      : { $$.type = NONE; }
                | varListType LEXER_COLON varDecList { $$ = $1; if( $3.type != NONE) { addNext(&$$, $3); } };
varListType     : final LEXER_TYPE varList { if($1.type == NONE) { $$ = makeNode2(VAR_LIST, $2, $3); } else { $$ = makeNode3(VAR_LIST, $1, $2, $3); } };
final           : { $$.type = NONE; }
                | LEXER_FINAL { $$ = $1; };
varList         : ident LEXER_COMMA varList { $$ = $1; addNext(&$$, $3); }
                | ident LEXER_ASSIGN expr LEXER_COMMA varList { $$ = makeNode2(ASSIGN, $1, $3); addNext(&$$, $5); }
                | ident { $$ = $1; }
                | ident LEXER_ASSIGN expr { $$ = makeNode2(ASSIGN, $1, $3); };

ident           : LEXER_IDENT { $$ = $1; }
                | LEXER_IDENT LEXER_O_BRACKET simpleExpr LEXER_C_BRACKET { $$ = makeNode2(VAR, $1, $3); };
stmtList        : stmtList statement  { $$ = makeNode2(STATEMENT_LIST, $1, $2); }
                | statement  { $$ = makeNode1(STATEMENT_LIST, $1); };
statement       : assignStmt LEXER_COLON  { $$ = makeNode1(ASSIGN_STMT, $1); }
                | compStmt { $$ = $1; }
                | ifStmt { $$ = $1; }
                | whileStmt { $$ = $1; }
                | forStmt { $$ = $1; }
                | ioStmt LEXER_COLON { $$ = $1; };
assignStmt      : ident LEXER_ASSIGN expr { $$ = makeNode2(ASSIGN, $1, $3); }
                | ident LEXER_PLUS_PLUS { $$ = makeNode2(EXPR, $1, $2); }
                | ident LEXER_MINUS_MINUS { $$ = makeNode2(EXPR, $1, $2); };
ifStmt          : LEXER_IF LEXER_O_PAREN expr LEXER_C_PAREN statement elsePart { if($6.type == NONE) { $$ = makeNode2(IF, $3, $5); } else { $$ = makeNode3(IF, $3, $5, $6); } };
elsePart        : { $$.type = NONE; }
                | LEXER_ELSE statement { $$ = $2; };
whileStmt       : LEXER_WHILE LEXER_O_PAREN expr LEXER_C_PAREN statement { $$ = makeNode2(WHILE, $3, $5); };
forStmt         : LEXER_FOR LEXER_O_PAREN assignStmt LEXER_COLON expr LEXER_COLON assignStmt LEXER_C_PAREN statement { $$ = makeNode4(FOR, $3, $5, $7, $9); };
ioStmt          : LEXER_IOFUNC LEXER_O_PAREN paramList LEXER_C_PAREN { $$ = makeNode2(IOFUNCCALL, $1, $3); };
paramList       : paramList LEXER_COMMA expr { $$ = makeNode2(PARAM_LIST, $1, $3); }
                | paramList LEXER_COMMA LEXER_STRING { $$ = makeNode2(PARAM_LIST, $1, $3); }
                | expr { $$ = $1; }
                | LEXER_STRING { $$ = $1; };
expr            : simpleExpr relOp simpleExpr { $$ = makeNode3(EXPR, $1, $2, $3); }
                | simpleExpr { $$ = $1; };
simpleExpr      : term addOp simpleExpr { $$ = makeNode3(EXPR, $1, $2, $3); }
                | term { $$ = $1; };
term            : term mulOp factor { $$ = makeNode3(EXPR, $1, $2, $3); }
                | factor { $$ = $1; };
factor          : LEXER_NUMBER { $$ = $1; }
                | LEXER_TRUTH_VAL { $$ = $1; }
                | ident { $$ = $1; }
                | LEXER_EXCLAMATION_MARK factor { $$ = makeNode2(EXPR, $1, $2); }
                | LEXER_MINUS factor { $$ = makeNode2(EXPR, $1, $2); }
                | LEXER_O_PAREN factor LEXER_C_PAREN { $$ = $2; };
relOp           : LEXER_SMALLER { $$ = $1; } | LEXER_SMALLER_EQUALS { $$ = $1; } | LEXER_GREATER { $$ = $1; } | LEXER_GREATER_EQUALS { $$ = $1; } | LEXER_NOT_EQUALS { $$ = $1; } | LEXER_EQUALS { $$ = $1; };
addOp           : LEXER_PLUS { $$ = $1; } | LEXER_MINUS { $$ = $1; } | LEXER_OR { $$ = $1; };
mulOp           : LEXER_STAR { $$ = $1; } | LEXER_SLASH { $$ = $1; } | LEXER_PERCENT { $$ = $1; } | LEXER_AND { $$ = $1; };

%%

const char* printableOperator(int op) {
    static const char* opToString[] = { "PLUS", "MINUS", "MUL", "DIV", "MOD", "LT", "LE", "GT", "GE", "EQ", "NE", "AND", "OR", "PLUSPLUS", "MINUSMINUS", "NOT",
 };
    return opToString[op];
}

const char* printableNodeType(int nodeType) {
    static const char* nodeTypeToString[] = { "COMP_STMT", "ASSIGN", "ASSIGN_STMT", "IF", "FOR", "WHILE", "STATEMENT", "STATEMENT_LIST", "CONST", "VAR", "VAR_LIST", "TYPE", "EXPR", "INT_CONST", "REAL_CONST", "BOOL_CONST", "STRING_CONST", "IDENTIFIER", "OP", "IOFUNC", "IOFUNCCALL", "PARAM_LIST", "FINAL", "NONE" };
    return nodeTypeToString[nodeType];
}

void printParseTree(node root, int depth) {
    int type = root.type;

    if(type == IDENTIFIER || type == IOFUNC || type == TYPE || type == STRING_CONST || type == FINAL) {
        for(int i = 0; i < depth; ++i) { printf("  "); }
        printf("type=%s val=%s\n", printableNodeType(type), root.identifier);
    } else if(type == BOOL_CONST || type == INT_CONST) {
        for(int i = 0; i < depth; ++i) { printf("  "); }
        printf("type=%s val=%d\n", printableNodeType(type), root.iValue);
    } else if(type == OP) {
        for(int i = 0; i < depth; ++i) { printf("  "); }
        printf("type=%s val=%s\n", printableNodeType(type), printableOperator(root.iValue));
    } else  { //we have body
        for(int i = 0; i < depth; ++i) { printf("  "); }
        printf("type=%s with children:\n", printableNodeType(type));
        node* cur = root.body;
        while(cur->type != NONE) {
            printParseTree(*cur, depth+1);
            ++cur;
        }
    }
    if(root.next != NULL) {
        printParseTree(*root.next, depth);
    }
}

void parseTreeToSource(node root, int depth) {
    int type = root.type;
    if(type == IDENTIFIER) {
        printf("%s", root.identifier);
    } else if(type == IOFUNC) {
        printf("%s", root.identifier);
    } else if(type == TYPE) {
        printf("%s", root.identifier);
    } else if(type == STRING_CONST) {
        printf("\"%s\"", root.identifier);
    } else if(type == BOOL_CONST) {
        if(root.iValue != 0) {
            printf("true");
        } else {
            printf("false");
        }
    } else if(type == INT_CONST) {
        printf("%d", root.iValue);
    } else if(type == OP) {
        if(root.iValue == PLUS) {
            printf("+");
        } else if(root.iValue == MINUS) {
            printf("-");
        } else if(root.iValue == MUL) {
            printf("*");
        } else if(root.iValue == DIV) {
            printf("/");
        } else if(root.iValue == MOD) {
            printf("%%");
        } else if(root.iValue == LT) {
            printf("<");
        } else if(root.iValue == LE) {
            printf("<=");
        } else if(root.iValue == GT) {
            printf(">");
        } else if(root.iValue == GE) {
            printf(">=");
        } else if(root.iValue == EQ) {
            printf("==");
        } else if(root.iValue == NE) {
            printf("!=");
        } else if(root.iValue == AND) {
            printf("&&");
        } else if(root.iValue == OR) {
            printf("||");
        } else if(root.iValue == PLUSPLUS) {
            printf("++");
        } else if(root.iValue == MINUSMINUS) {
            printf("--");
        } else if(root.iValue == NOT) {
            printf("!");
        }
    } else if(type == IOFUNCCALL) {
        for(int i = 0; i < depth; ++i) { printf("  "); }
        parseTreeToSource(root.body[0], depth);
        printf("(");
        node* cur = root.body + 1;
        while(cur->type != NONE) {
            parseTreeToSource(*cur, depth);
            ++cur;
        }
        printf(");\n");
    } else if(type == VAR) {
        node* cur = root.body;
        assert(cur->type != NONE);
        parseTreeToSource(*cur, depth);
        ++cur;
        while(cur->type != NONE) {
            printf("[");
            parseTreeToSource(*cur, depth);
            printf("]");
            ++cur;
        }
    } else if(type == EXPR) {
        if(root.body[2].type != NONE) { //a three term expression, like a + b
            if(root.body[0].type == EXPR) { printf("("); }
            parseTreeToSource(root.body[0], depth);
            if(root.body[0].type == EXPR) { printf(")"); }
            printf(" ");
            parseTreeToSource(root.body[1], depth);
            printf(" ");
            if(root.body[2].type == EXPR) { printf("("); }
            parseTreeToSource(root.body[2], depth);
            if(root.body[2].type == EXPR) { printf(")"); }
        } else {
            parseTreeToSource(root.body[0], depth);
            parseTreeToSource(root.body[1], depth);
        }
    } else if(type == ASSIGN) {
        parseTreeToSource(root.body[0], depth);
        printf(" = ");
        parseTreeToSource(root.body[1], depth);
    } else if(type == FOR) {
        for(int i = 0; i < depth; ++i) { printf("  "); }
        printf("for(");
        parseTreeToSource(root.body[0], depth);
        printf("; ");
        parseTreeToSource(root.body[1], depth);
        printf("; ");
        parseTreeToSource(root.body[2], depth);
        printf(")\n");
        parseTreeToSource(root.body[3], depth+(root.body[3].type != COMP_STMT ? 1 : 0));
    } else if(type == WHILE) {
        for(int i = 0; i < depth; ++i) { printf("  "); }
        printf("while(");
        parseTreeToSource(root.body[0], depth);
        printf(")\n");
        parseTreeToSource(root.body[1], depth + (root.body[1].type != COMP_STMT ? 1 : 0));
    } else if(type == ASSIGN_STMT) {
        for(int i = 0; i < depth; ++i) { printf("  "); }
        parseTreeToSource(root.body[0], depth);
        printf(";\n");
    } else if(type == IF) {
        for(int i = 0; i < depth; ++i) { printf("  "); }
        printf("if(");
        parseTreeToSource(root.body[0], depth);
        printf(")\n");
        parseTreeToSource(root.body[1], depth+(root.body[1].type != COMP_STMT ? 1 : 0));
        if(root.body[2].type != NONE) {
            for(int i = 0; i < depth; ++i) { printf("  "); }
            printf("else\n");
            parseTreeToSource(root.body[2], depth+(root.body[2].type != COMP_STMT ? 1 : 0));
        }
    } else if(type == COMP_STMT) {
        for(int i = 0; i < depth; ++i) { printf("  "); }
        printf("{\n");
        node* cur = root.body;
        while(cur->type != NONE) {
            parseTreeToSource(*cur, depth+1);
            ++cur;
        }
        for(int i = 0; i < depth; ++i) { printf("  "); }
        printf("}\n");
        cur = &root;
        if(cur->next != NULL) {
            parseTreeToSource(*cur->next, depth);
        }
    } else if(type == STATEMENT_LIST) {
        node* cur = root.body;
        while(cur->type != NONE) {
            parseTreeToSource(*cur, depth);
            ++cur;
        }
        assert(root.next == NULL);
    } else if(type == VAR_LIST) {
        for(int i = 0; i < depth; ++i) { printf("  "); }
        int type_pos = 0;
        if(root.body[2].type != NONE) { //we have "final" first
            printf("final ");
            ++type_pos;
        }
        parseTreeToSource(root.body[type_pos], depth);
        printf(" ");
        parseTreeToSource(root.body[type_pos+1], depth);
        node* cur = &root.body[type_pos+1];
        while(cur->next != NULL) {
            printf(", ");
            parseTreeToSource(*cur->next, depth);
            cur = cur->next;
        }
        printf(";\n");
        cur = &root;
        if(cur->next != NULL) {
            parseTreeToSource(*cur->next, depth);
        }
    } else  {
        assert(type == PARAM_LIST);
        parseTreeToSource(root.body[0], depth);
        printf(", ");
        parseTreeToSource(root.body[1], depth);
    }
}

int main() {
    int res = yyparse();
    if(res == 0) {
        //printParseTree(tree_root, 0);
        printf("\n\n---------Reverse Source--------\n");
        parseTreeToSource(tree_root, 0);
    }
    return res;
}

int yyerror(const char* s) {
    fprintf(stderr, "%s, line ~%d\n", s, yylineno);
    return 0;
}

node makeNode1(int type, node body1) {
    assert(body1.type != NONE);
    node res;
    res.type = type;
    res.body = (node*)malloc(2*sizeof(node));
    res.body[0] = body1;
    res.body[1].type = NONE;
    res.next = NULL;
    return res;
}

node makeNode2(int type, node body1, node body2) {
    assert(body1.type != NONE && body2.type != NONE);
    node res;
    res.type = type;
    res.body = (node*)malloc(3*sizeof(node));
    res.body[0] = body1;
    res.body[1] = body2;
    res.body[2].type = NONE;
    res.next = NULL;
    return res;
}

node makeNode3(int type, node body1, node body2, node body3) {
    assert(body1.type != NONE && body2.type != NONE && body3.type != NONE);
    node res;
    res.type = type;
    res.body = (node*)malloc(4*sizeof(node));
    res.body[0] = body1;
    res.body[1] = body2;
    res.body[2] = body3;
    res.body[3].type = NONE;
    res.next = NULL; 
    return res;
}

node makeNode4(int type, node body1, node body2, node body3, node body4) {
    assert(body1.type != NONE && body2.type != NONE && body3.type != NONE && body4.type != NONE);
    node res;
    res.type = type;
    res.body = (node*)malloc(5*sizeof(node));
    res.body[0] = body1;
    res.body[1] = body2;
    res.body[2] = body3;
    res.body[3] = body4;
    res.body[4].type = NONE;
    res.next = NULL; 
    return res;
}

node leafNodeIdent(int type, char* identifier) {
    node result;
    result.type = type;
    result.identifier = identifier;
    result.next = NULL; 
    return result;
}

node leafNodeInt(int type, int value) {
    node result;
    result.type = type;
    result.iValue = value;
    result.next = NULL; 
    return result;
}

void addNext(node* to, node next) {
    free(to->next);
    to->next = (node*)malloc(sizeof(node));
    *to->next = next;
}

char* string_buf = NULL;
int   string_buf_free_from;
int   string_buf_len;
void addToString(char* toAdd) {
    if(string_buf == NULL) {
        string_buf_free_from = 0;
        string_buf_len = 0;
    }
    int newUsed = string_buf_free_from + strnlen(toAdd, 1024);
    if(newUsed >= string_buf_len) {
        char* newbuf = malloc(newUsed * 2 + 1);
        if(string_buf != NULL) {
            memcpy(newbuf, string_buf, string_buf_free_from*sizeof(char));
        }
        free(string_buf);
        string_buf_len = newUsed * 2;
        string_buf = newbuf;
    }
    memcpy(string_buf + string_buf_free_from, toAdd, strnlen(toAdd, 1024));
    string_buf_free_from += strnlen(toAdd, 1024);
    string_buf[string_buf_free_from] = '\0';
}
